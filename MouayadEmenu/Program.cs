using Microsoft.EntityFrameworkCore;
using MouayadEmenu.Data;
using MouayadEmenu.repositories;
using MouayadEmenu.Services;
using MouayadEmenu.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.
builder.Services.AddDbContext<DataContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddControllers(
    options => options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true);
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddScoped<IlanguageService, LanguageService>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IVariantService, VariantService>();
builder.Services.AddScoped<IPropertyService, PropertyService>();
builder.Services.AddScoped<LanguageRepository>();
builder.Services.AddScoped<ProductRepository>();
builder.Services.AddScoped<VariantRepository>();
builder.Services.AddScoped<PropertyRepository>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if(app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
