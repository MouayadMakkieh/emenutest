﻿using MouayadEmenu.Models;
using MouayadEmenu.repositories;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Services
{
    public class LanguageService : IlanguageService
    {
        private readonly LanguageRepository _languageRepository;
        public LanguageService(LanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }
        //bool IsLanguageExist(Guid id)
        //{
        //    Language? language = _languageRepository.GetLanguageById(id);
        //    if (language == null)
        //        return false;
        //    return true;
        //}
        public async Task<ServiceResponse> GetAllLanguages()
        {
            List<GetLanguageDto> languages = await _languageRepository.GetAllLanguages();
            if (languages.Count == 0)
                return new ServiceResponse { success = false };
            return new ServiceResponse { data = languages, success = true };
        }
        public ServiceResponse GetLanguageById(Guid id)
        {
            GetLanguageDto? language = _languageRepository.GetLanguageDtoById(id);
            if (language == null)
                return new ServiceResponse { success = false, data = "no language found" };
            return new ServiceResponse { success = true, data = language };
        }
        public ServiceResponse PostLanguage(PostLanguageDto postLanguage)
        {
            GetLanguageDto language = _languageRepository.AddNewLanguage(postLanguage);
            return new ServiceResponse { success = true, data = language };
        }
        public ServiceResponse PutLanguage(Guid id, PostLanguageDto postLanguage)
        {
            Language? language = _languageRepository.GetLanguageById(id);
           if(language == null)
                return new ServiceResponse { success = false, message = "no language found" };
           GetLanguageDto getLanguageDto = _languageRepository.UpdateLanguageInformation(language, postLanguage);
           return new ServiceResponse { success = true,data = getLanguageDto };
        }
        public ServiceResponse DeleteLanguage(Guid id)
        {
            Language? language = _languageRepository.GetLanguageById(id);
            if (language == null)
                return new ServiceResponse { success = false, message = "no language found" };
            _languageRepository.Deletelanguage(language);
            return new ServiceResponse { success = true, message = "language deleted" };
        }
    }
}
