﻿using MouayadEmenu.Models;
using MouayadEmenu.repositories;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Services
{
    public class PropertyService : IPropertyService
    {
        private readonly PropertyRepository _propertyRepository;
        public PropertyService(PropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }
        public async Task<ServiceResponse> GetAllProperties()
        {
            List<GetPropertyDto> properties = await _propertyRepository.GetAllProperty();
            if (properties.Count == 0)
                return new ServiceResponse { success = false };
            return new ServiceResponse { data = properties, success = true };
        }

        public ServiceResponse GetPropertyById(Guid id)
        {
            GetPropertyDto? property = _propertyRepository.GetPropertyDtoById(id);
            if (property == null)
                return new ServiceResponse { success = false, message = "no property found" };
            return new ServiceResponse { success = true, data = property };
        }

        public ServiceResponse PostProperty(string PropertyName)
        {
            GetPropertyDto? property = _propertyRepository.AddNewProperty(PropertyName);
            return new ServiceResponse { success = true, data = property };
        }

        public ServiceResponse PutProperty(Guid id, string PropertyName)
        {
            Property? property = _propertyRepository.GetPropertyById(id);
            if (property == null)
                return new ServiceResponse { success = false, message = "no property found" };
            GetPropertyDto getPropertyDto = _propertyRepository.UpdatePropertyName(property, PropertyName);
            return new ServiceResponse { success = true, data = getPropertyDto };
        }

        public ServiceResponse DeleteProperty(Guid id)
        {
            Property? property = _propertyRepository.GetPropertyById(id);
            if (property == null)
                return new ServiceResponse { success = false, message = "no property found" };
            _propertyRepository.DeleteProperty(property);
            return new ServiceResponse { success = true, message = "property deleted" };
        }

        public async Task<ServiceResponse> GetPropertyItemsByPropertyId(Guid id)
        {
            Property? property = _propertyRepository.GetPropertyById(id);
            if (property == null)
                return new ServiceResponse { success = false, message = "no property found" };
            List<GetPropertyItemDto> items = await _propertyRepository.GetPropertyItemsByPropertyId(id);
            if (items.Count == 0)
                return new ServiceResponse { success = false, data = items };
            return new ServiceResponse { success = true,data = items};
        }

        public ServiceResponse GetPropertyItemById(Guid id)
        {
            GetPropertyItemDto? propertyItem = _propertyRepository.GetPropertyItemDtoById(id);
            if (propertyItem == null)
                return new ServiceResponse { success = false, message = "no property found" };
            return new ServiceResponse { success = true, data = propertyItem };
        }

        public ServiceResponse PostPropertyItem(PostPropertyItemDto postPropertyItemDto)
        {
            Property ? property = _propertyRepository.GetPropertyById(postPropertyItemDto.Propertyid);
            if (property == null)
                return new ServiceResponse { success = false, message = "no property found" };
            GetPropertyItemDto propertyItem = _propertyRepository.AddNewPropertyItem(postPropertyItemDto);
            return new ServiceResponse { success = true,data=propertyItem};
        }

        public ServiceResponse PutPropertyItem(Guid id, PostPropertyItemDto postPropertyItemDto)
        {
            Property? property = _propertyRepository.GetPropertyById(postPropertyItemDto.Propertyid);
            if (property == null)
                return new ServiceResponse { success = false, message = "no property found" };
            PropertyItem? propertyItem = _propertyRepository.GetPropertyItemById(id);
            if (propertyItem == null)
                return new ServiceResponse { success = false, message = "no property item found" };
            GetPropertyItemDto getPropertyItemDto = _propertyRepository.UpdatePropertyItemInformation(propertyItem, postPropertyItemDto);
            return new ServiceResponse { success = true,data = getPropertyItemDto};
        }

        public ServiceResponse DeletePropertyItem(Guid id)
        {
            PropertyItem? propertyItem = _propertyRepository.GetPropertyItemById(id);
            if (propertyItem == null)
                return new ServiceResponse { success = false, message = "no property item found" };
            return new ServiceResponse { success = true,data =  propertyItem};
        } 
    }
}
