﻿using MouayadEmenu.Models;

namespace MouayadEmenu.Services.Interfaces
{
    public interface IlanguageService
    {
        Task<ServiceResponse> GetAllLanguages();
        ServiceResponse GetLanguageById(Guid id);
        ServiceResponse PostLanguage(PostLanguageDto postLanguage);
        ServiceResponse PutLanguage(Guid id, PostLanguageDto postLanguage);
        ServiceResponse DeleteLanguage(Guid id);



    }
}
