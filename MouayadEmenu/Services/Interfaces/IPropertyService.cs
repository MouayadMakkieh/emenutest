﻿using MouayadEmenu.Models;

namespace MouayadEmenu.Services.Interfaces
{
    public interface IPropertyService
    {
        Task<ServiceResponse> GetAllProperties();
        ServiceResponse GetPropertyById(Guid id);
        ServiceResponse PostProperty(string PropertyName);
        ServiceResponse PutProperty(Guid id,string PropertyName);
        ServiceResponse DeleteProperty(Guid id);
        Task<ServiceResponse> GetPropertyItemsByPropertyId(Guid id);
        ServiceResponse GetPropertyItemById(Guid id);
        ServiceResponse PostPropertyItem(PostPropertyItemDto propertyItem);
        ServiceResponse PutPropertyItem(Guid id,PostPropertyItemDto propertyItem);
        ServiceResponse DeletePropertyItem(Guid id);
    }
}
