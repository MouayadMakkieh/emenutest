﻿using MouayadEmenu.Models;

namespace MouayadEmenu.Services.Interfaces
{
    public interface IVariantService
    {
        public ServiceResponse PostVariant(PostVariantDto postVariantDto);
        public ServiceResponse GetVariantById(Guid variantId);
        public ServiceResponse PutVariant(Guid id,PostVariantDto postVariantDto);
        public ServiceResponse DeleteVariant(Guid id);
        public ServiceResponse AddImageForVariant(Guid VariantId,Guid imageId);
        public ServiceResponse AddPropertyItemForVariant(PostVariantPropertyDto postVariantPropertyDto);
        public Task<ServiceResponse> GetImagesByVariantId(Guid variantId);
        public Task<ServiceResponse> GetPropertyItemsByVariantId(Guid variantId);
        public ServiceResponse DeleteVariantProperty(Guid variantId, Guid propertyItemId);
        public ServiceResponse RemoveImageFromVariant(Guid image);
        public Task<PaginationServiceResponse> GetVariants(FilterModel filterModel);


    }
}
