﻿using MouayadEmenu.Models;

namespace MouayadEmenu.Services.Interfaces
{
    public interface IProductService
    {
        Task<ServiceResponse> PostProduct(PostProductDto postProductDto);
        ServiceResponse GetProductById(Guid id);
        ServiceResponse PutProduct(Guid id, PutProductDto putProductDto);
        ServiceResponse DeleteProduct(Guid id);
        Task<ServiceResponse> PostProductImage(Guid ProductId,PostProductImageDto postProductImageDto);
        Task<ServiceResponse> GetProductImagesByProductId(Guid id);
        ServiceResponse GetPoductImageById(Guid id);
        ServiceResponse DeleteProductImage(Guid id);
        ServiceResponse PostProductLocalizer(Guid id,PostProductLocalizerDto postProductLocalizerDto);
        Task<ServiceResponse> GetProductLocalizersByProductId(Guid id);
        ServiceResponse GetProductLocalizerById(Guid id);
        ServiceResponse DeleteProductLocalizer(Guid id);
        ServiceResponse PutProductLocalizer(Guid id,PutProductLocalizerDto putProductLocalizerDto);
        PaginationServiceResponse GetProducts(FilterModel filterModel);
    }
}
