﻿using MouayadEmenu.Models;
using MouayadEmenu.repositories;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Services
{
    public class VariantService : IVariantService
    {
        private readonly VariantRepository _variantRepository;
        private readonly ProductRepository _productRepository;
        private readonly PropertyRepository _propertyRepository;

        public VariantService(VariantRepository variantRepository, ProductRepository productRepository, PropertyRepository propertyRepository)
        {
            _variantRepository = variantRepository;
            _productRepository = productRepository;
            _propertyRepository = propertyRepository;
        }

        public ServiceResponse PostVariant(PostVariantDto postVariantDto)
        {
            Product product = _productRepository.GetProductById(postVariantDto.ProductId);
            if(product == null) 
                return new ServiceResponse { success = false,message = "invalid productId" };
            GetVariantDto variant = _variantRepository.AddVariant(postVariantDto);
            return new ServiceResponse { success = true,data = variant};
        }
        public async Task<PaginationServiceResponse> GetVariants(FilterModel filterModel)
        {
            List<GetVariantDto> variants = await _variantRepository.GetVariants(filterModel);
            if (variants.Count == 0)
                return new PaginationServiceResponse { Success = false };
            int totalPages = 0;
            if (variants.Count % filterModel.PageSize == 0)
                totalPages = variants.Count / filterModel.PageSize;
            else
                totalPages = variants.Count / filterModel.PageSize + 1;
            return new PaginationServiceResponse { Success = true, Data = variants, DataSize = variants.Count, TotalPages = totalPages };
        }
        public ServiceResponse GetVariantById(Guid variantId)
        {
            GetVariantDto? variant = _variantRepository.GetVariantDtoById(variantId);
            if (variant == null)
                return new ServiceResponse { success = false, message = "no variant found" };
            return new ServiceResponse { success = true,data = variant};
        }
        public ServiceResponse PutVariant(Guid id, PostVariantDto postVariantDto)
        {
            Variant? variant = _variantRepository.GetVariantById(id);
            if (variant == null)
                return new ServiceResponse { success = false, message = "no variant found" };
            GetVariantDto getVariantDto = _variantRepository.UpdateVariantInformation(variant, postVariantDto);
            return new ServiceResponse { success = true,data = getVariantDto};
        }
        public ServiceResponse DeleteVariant(Guid id)
        {
            Variant? variant = _variantRepository.GetVariantById(id);
            if (variant == null)
                return new ServiceResponse { success = false, message = "no variant found" };
            _variantRepository.DeleteVariant(variant);
            return new ServiceResponse { success = true, message = "variant deleted" };
        }

        public ServiceResponse AddImageForVariant(Guid VariantId, Guid imageId)
        {
            ProductImage? image = _productRepository.GetProductImageById(imageId);
            if (image == null)
                return new ServiceResponse { success = false, message = "image not found" };
            Variant? variant = _variantRepository.GetVariantById(VariantId);
            if (variant == null)
                return new ServiceResponse { success = false, message = "variant not found" };
            GetProductImageDto? getImageDto = _variantRepository.AddImageForVariant(VariantId, image);
            return new ServiceResponse { success = true,data = getImageDto};
        }

        public ServiceResponse AddPropertyItemForVariant(PostVariantPropertyDto postVariantPropertyDto)
        {
            Variant? variant = _variantRepository.GetVariantById(postVariantPropertyDto.VariantId);
            if (null == variant)
                return new ServiceResponse { success = false, message = "variant not found" };
            PropertyItem? propertyItem = _propertyRepository.GetPropertyItemById(postVariantPropertyDto.PropertyItemId);
            if (propertyItem == null)
                return new ServiceResponse { success = false, message = "no property item found" };
            GetVariantPropertyDto getVariantPropertyDto = _variantRepository.AddPropertyItemForVariant(postVariantPropertyDto);
            return new ServiceResponse { success = true, data = getVariantPropertyDto};
        }

        public ServiceResponse DeleteVariantProperty(Guid variantId, Guid propertyItemId)
        {
            VariantProperty? variantProperty = _variantRepository.GetVariantProperty(variantId, propertyItemId);
            if (null == variantProperty)
                return new ServiceResponse { success = false, message = "no variant property found" };
            _variantRepository.DeletePropertyItemForVariant(variantProperty);
            return new ServiceResponse { success = true, message = "variant property deleted" };
        }

        public async Task<ServiceResponse> GetImagesByVariantId(Guid variantId)
        {
            Variant? variant = _variantRepository.GetVariantById(variantId);
            if (variant == null)
                return new ServiceResponse { success = false, message = "no variant found" };
            List<GetProductImageDto> images = await _variantRepository.GetProductImagesForVariant(variantId);
            if(images.Count == 0)
                return new ServiceResponse { success = false, data = images};
            return new ServiceResponse { success = true,data = images};
        }
        public ServiceResponse RemoveImageFromVariant(Guid imageId)
        {
            ProductImage? image = _productRepository.GetProductImageById(imageId);
            if (image == null)
                return new ServiceResponse { success = false, message = "no image found" };
            GetProductImageDto getProductImageDto = _variantRepository.DeleteImageForVariant(image);
            return new ServiceResponse { success = true, data = image};
        }
        public async Task<ServiceResponse> GetPropertyItemsByVariantId(Guid variantId)
        {
            Variant? variant = _variantRepository.GetVariantById(variantId);
            if (variant == null)
                return new ServiceResponse { success = false, message = "no variant found" };
            List<GetPropertyItemDto> PropertyItems = await _variantRepository.GetPropertyItemsForVariant(variantId);
            if (PropertyItems.Count == 0)
                return new ServiceResponse { success = false, data = PropertyItems };
            return new ServiceResponse { success = true, data = PropertyItems };
        }
    }
}
