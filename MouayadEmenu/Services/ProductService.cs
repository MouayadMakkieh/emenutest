﻿using MouayadEmenu.Models;
using MouayadEmenu.repositories;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Services
{
    public class ProductService : IProductService
    {
        private readonly ProductRepository _productRepository;
        private readonly LanguageRepository _languageRepository;

        public ProductService(ProductRepository productRepository, LanguageRepository languageRepository)
        {
            _productRepository = productRepository;
            _languageRepository = languageRepository;
        }

        public async Task<ServiceResponse> PostProduct(PostProductDto postProductDto)
        {
            GetProductDto? getProductDto =await  _productRepository.AddProduct(postProductDto);
            if (postProductDto == null)
                return new ServiceResponse
                {
                    success = false
                    ,
                    message = "Invalid Image Please Upload image smaller than 3 Mb And with one of these extensions : \".jpg\", \".jpeg\", \".png\", \".gif\""
                };
            return new ServiceResponse { success = true, data = getProductDto };
        }
        public ServiceResponse GetProductById(Guid id)
        {
            GetProductDto? product = _productRepository.GetProductDtoById(id);
            if (product == null)
                return new ServiceResponse { success = false, message = "no Product Found" };
            return new ServiceResponse { success = true,data = product };
        }
        public PaginationServiceResponse GetProducts(FilterModel filterModel)
        {
            List<GetProductDto> products = _productRepository.GetProducts(filterModel);
            if (products.Count == 0)
                return new PaginationServiceResponse { Success = false };
            int totalPages = 0;
            if(products.Count%filterModel.PageSize==0)
                totalPages = products.Count/filterModel.PageSize;
            else
                totalPages = products.Count/filterModel.PageSize + 1;
            return new PaginationServiceResponse { Success = true,Data=products,DataSize = products.Count,TotalPages = totalPages};
        }
        public ServiceResponse PutProduct(Guid id, PutProductDto putProductDto)
        {
            Product? product = _productRepository.GetProductById(id);
            if (product == null)
                return new ServiceResponse { success = false, message = "no product found" };
            GetProductDto getProductDto = _productRepository.UpdateProductInformation(product, putProductDto);
            return new ServiceResponse { success = true,data= getProductDto };
        }
        public ServiceResponse DeleteProduct(Guid id)
        {
            Product? product = _productRepository.GetProductById(id);
            if (product == null)
                return new ServiceResponse { success = false, message = "no product found" };
            _productRepository.DeleteProduct(product);
            return new ServiceResponse { success = true, message = "product Deleted" };
        }
        public async Task<ServiceResponse> PostProductImage(Guid ProductId,PostProductImageDto postProductImageDto)
        {
            Product? product = _productRepository.GetProductById(ProductId);
            if (product == null)
                return new ServiceResponse { success = false, message = "Invalid product Id" };
            GetProductImageDto? productImage = await _productRepository.AddProductImage(ProductId, postProductImageDto);
            if (productImage == null)
                return new ServiceResponse
                {
                    success = false
                    ,
                    message = "Invalid Image Please Upload image smaller than 3 Mb And with one of these extensions : \".jpg\", \".jpeg\", \".png\", \".gif\""
                };
            _productRepository.MakeImagesNotMainForProductExceptImage(ProductId, productImage.Id);
            return new ServiceResponse { success = true,data = productImage };
        }
        public ServiceResponse GetPoductImageById(Guid id)
        {
            GetProductImageDto? productImage = _productRepository.GetProductImageDtoById(id);
            if (productImage == null)
                return new ServiceResponse() { success = false, message = "not product image found" };
            return new ServiceResponse { success = true, data = productImage };
        }
        public async Task<ServiceResponse> GetProductImagesByProductId(Guid id)
        {
            Product? product = _productRepository.GetProductById(id);
            if (product == null)
                return new ServiceResponse { success = false, message = "no product found" };
            List<GetProductImageDto> images = await _productRepository.GetProductImagesForProduct(id);
            if (images.Count == 0)
                return new ServiceResponse { success = false, data = images };
            return new ServiceResponse { success = true, data = images };
        }
        public ServiceResponse DeleteProductImage(Guid id)
        {
            ProductImage? productImage = _productRepository.GetProductImageById(id);
            if (productImage == null)
                return new ServiceResponse { success = false, message = "no product image found" };
            _productRepository.DeleteProductImage(productImage);
            return new ServiceResponse { success = true, message = "product image Deleted" };
        }
        public ServiceResponse PostProductLocalizer(Guid ProductId,PostProductLocalizerDto postProductLocalizerDto)
        {
            Language? language = _languageRepository.GetLanguageById(postProductLocalizerDto.LanguageId);
            if (language == null)
                return new ServiceResponse { success = false, message = "no language found" };
            Product? product = _productRepository.GetProductById(ProductId);
            if (product == null)
                return new ServiceResponse { success = false, message = "no product found" };
            GetProductLocalizerDto productLocalizer = _productRepository.AddProductLocalizer(ProductId, postProductLocalizerDto);
            return new ServiceResponse { success = true,data = productLocalizer };
        }

        public ServiceResponse GetProductLocalizerById(Guid id)
        {
            GetProductLocalizerDto? productLocalizer = _productRepository.GetProductLocalizerDtoById(id);
            if (productLocalizer == null)
                return new ServiceResponse { success = false, message = "no Product Localizer found" };
            return new ServiceResponse { success = true,data = productLocalizer };
        }

        public async Task<ServiceResponse> GetProductLocalizersByProductId(Guid id)
        {
            Product? product = _productRepository.GetProductById(id);
            if (product == null)
                return new ServiceResponse { success = false, message = "no Product found" };
            List<GetProductLocalizerDto> localizer = await _productRepository.GetProductLocalizersForProduct(id);
            if(localizer.Count == 0)
                return new ServiceResponse { success = false, data = localizer };
            return new ServiceResponse { success = true,data = localizer };
        }

        public ServiceResponse PutProductLocalizer(Guid id, PutProductLocalizerDto putProductLocalizerDto)
        {
            ProductLocalizer? productLocalizer = _productRepository.GetProductLocalizerById(id);
            if (productLocalizer == null)
                return new ServiceResponse() { success = false, message = "no product localizer found" };
            Language? language = _languageRepository.GetLanguageById(putProductLocalizerDto.LanguageId);
            if (language == null)
                return new ServiceResponse { success = false, message = "no language found" };
            Product? product = _productRepository.GetProductById(putProductLocalizerDto.ProductId);
            if (product == null)
                return new ServiceResponse { success = false, message = "no product found" };
            GetProductLocalizerDto getProductLocalizerDto = _productRepository.UpdateProductLocalizerInformation(productLocalizer, putProductLocalizerDto);
            return new ServiceResponse { success = true, data = getProductLocalizerDto };
        }
        public ServiceResponse DeleteProductLocalizer(Guid id)
        {
            ProductLocalizer? productLocalizer = _productRepository.GetProductLocalizerById(id);
            if (productLocalizer == null)
                return new ServiceResponse() { success = false, message = "no product localizer found" };
            _productRepository.DeleteProductLolizer(productLocalizer);
            return new ServiceResponse { success = true, message = "product localizer Deleted" };
        }


    }
}
