﻿using Microsoft.EntityFrameworkCore;
using MouayadEmenu.Models;

namespace MouayadEmenu.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Variant> Variants { get; set; }
        public DbSet<ProductLocalizer> ProductLocalizers { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<PropertyItem> PropertyItems { get; set; }
        public DbSet<VariantProperty> VariantProperties { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductImage>()
                .HasOne(p=>p.Product)
                .WithMany(p=>p.Images)
                .HasForeignKey(p=>p.ProductId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            modelBuilder.Entity<ProductImage>()
                .HasOne(p => p.Variant)
                .WithMany(p => p.Images)
                .HasForeignKey(p => p.VariantId)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
            modelBuilder.Entity<ProductLocalizer>()
                .HasOne(p => p.Product)
                .WithMany(p => p.Localizers)
                .HasForeignKey(p => p.ProductId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
            modelBuilder.Entity<ProductLocalizer>()
                .HasOne(p=>p.Language)
                .WithMany(l=>l.Localizers)
                .HasForeignKey(p=>p.LanguageId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            modelBuilder.Entity<Product>()
                .HasIndex(p => p.Name)
                .IsUnique();
            modelBuilder.Entity <Language>()
                .HasIndex(l=>l.EnglishName)
                .IsUnique();
            modelBuilder.Entity<Language>()
                .HasIndex(l=>l.NativeName)
                .IsUnique();
            modelBuilder.Entity<Property>()
                .HasIndex(p=>p.Name)
                .IsUnique();
            modelBuilder.Entity<Variant>()
                .HasIndex(p=>p.Name)
                .IsUnique();
            modelBuilder.Entity<ProductLocalizer>()
                .HasIndex(p=>p.LoacalizeName)
                .IsUnique();
            base.OnModelCreating(modelBuilder);
        }


    }
}
