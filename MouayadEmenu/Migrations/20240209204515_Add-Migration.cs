﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MouayadEmenu.Migrations
{
    /// <inheritdoc />
    public partial class AddMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Localizers_Languages_LanguageId",
                table: "Localizers");

            migrationBuilder.DropForeignKey(
                name: "FK_Localizers_Products_ProductId",
                table: "Localizers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Localizers",
                table: "Localizers");

            migrationBuilder.RenameTable(
                name: "Localizers",
                newName: "ProductLocalizers");

            migrationBuilder.RenameIndex(
                name: "IX_Localizers_ProductId",
                table: "ProductLocalizers",
                newName: "IX_ProductLocalizers_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_Localizers_LoacalizeName",
                table: "ProductLocalizers",
                newName: "IX_ProductLocalizers_LoacalizeName");

            migrationBuilder.RenameIndex(
                name: "IX_Localizers_LanguageId",
                table: "ProductLocalizers",
                newName: "IX_ProductLocalizers_LanguageId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductLocalizers",
                table: "ProductLocalizers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductLocalizers_Languages_LanguageId",
                table: "ProductLocalizers",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductLocalizers_Products_ProductId",
                table: "ProductLocalizers",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);


        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductLocalizers_Languages_LanguageId",
                table: "ProductLocalizers");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductLocalizers_Products_ProductId",
                table: "ProductLocalizers");


            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductLocalizers",
                table: "ProductLocalizers");

            migrationBuilder.RenameTable(
                name: "ProductLocalizers",
                newName: "Localizers");

            migrationBuilder.RenameIndex(
                name: "IX_ProductLocalizers_ProductId",
                table: "Localizers",
                newName: "IX_Localizers_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductLocalizers_LoacalizeName",
                table: "Localizers",
                newName: "IX_Localizers_LoacalizeName");

            migrationBuilder.RenameIndex(
                name: "IX_ProductLocalizers_LanguageId",
                table: "Localizers",
                newName: "IX_Localizers_LanguageId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Localizers",
                table: "Localizers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Localizers_Languages_LanguageId",
                table: "Localizers",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Localizers_Products_ProductId",
                table: "Localizers",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
