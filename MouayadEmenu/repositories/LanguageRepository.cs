﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MouayadEmenu.Data;
using MouayadEmenu.Models;

namespace MouayadEmenu.repositories
{
    public class LanguageRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        public LanguageRepository(DataContext dataContext,IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        public async Task<List<GetLanguageDto>> GetAllLanguages()
        {
            return await _dataContext.Languages
                .Select(l=>_mapper.Map<GetLanguageDto>(l))
                .ToListAsync();
        }
        public GetLanguageDto? GetLanguageDtoById(Guid id)
        {
            return _dataContext.Languages
                .Where(l => l.Id == id)
                .Select(l => _mapper.Map<GetLanguageDto>(l))
                .FirstOrDefault();
        }
        public Language? GetLanguageById(Guid id)
        {
            return _dataContext.Languages
                .FirstOrDefault(l=>l.Id == id);
        }
        public GetLanguageDto AddNewLanguage(PostLanguageDto postLanguageDto)
        {
            Language language = _mapper.Map<Language>(postLanguageDto);
            _dataContext.Languages.Add(language);
            _dataContext.SaveChanges();
            return _mapper.Map<GetLanguageDto>(language);
        }
        public GetLanguageDto UpdateLanguageInformation(Language language, PostLanguageDto postLanguageDto)
        {
           // Language? language = _dataContext.Languages.FirstOrDefault(l=>l.Id == id);
            _mapper.Map(postLanguageDto, language);
            // _dataContext.Entry(language).State = EntityState.Modified;
            _dataContext.Languages.Update(language);
            _dataContext.SaveChanges();
            return _mapper.Map<GetLanguageDto>(language);
        }
        public void Deletelanguage(Language language)
        {
            //Language? language = _dataContext.Languages.FirstOrDefault(l=>l.Id == id);
            _dataContext.Languages.Remove(language);
            _dataContext.SaveChanges();
        }

    }
}
