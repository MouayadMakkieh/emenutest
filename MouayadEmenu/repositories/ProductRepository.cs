﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MouayadEmenu.Data;
using MouayadEmenu.Models;
namespace MouayadEmenu.repositories
{
    public class ProductRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _environment;

        public ProductRepository(DataContext dataContext, IMapper mapper, IWebHostEnvironment environment)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _environment = environment;
        }
        bool IsImageValid(IFormFile image)
        {
            if (image == null)
                return false;
            int MaxSize = 1024 * 1024 * 3; //Size = 3 MB
            List<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".png", ".gif" };
            string extension = Path.GetExtension(image.FileName);
            if (!AllowedFileExtensions.Contains(extension, StringComparer.OrdinalIgnoreCase))
                return false;
            if (image.Length > MaxSize) return false;
            return true;
        }
        async Task<string> SaveImage(IFormFile image)
        {
            string path = Path.Combine(_environment.WebRootPath, "Images/", image.FileName);
            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                await image.CopyToAsync(stream);
                stream.Close();
            }
            return path;
        }
        public async Task<GetProductDto?> AddProduct(PostProductDto postProductDto)
        {
            Product product = _mapper.Map<Product>(postProductDto);
            product.Id = Guid.NewGuid();
            if (postProductDto.PostProductImageDto != null)
            {
                if (!IsImageValid(postProductDto.PostProductImageDto.Image))
                    return null;
                string imageUrl = await SaveImage(postProductDto.PostProductImageDto.Image);
                ProductImage productImage = _mapper.Map<ProductImage>(postProductDto.PostProductImageDto);
                productImage.Url = imageUrl;
                product.Images = new List<ProductImage> { productImage };
            }
            if(!postProductDto.PostProductLocalizers.IsNullOrEmpty()) 
            {
                product.Localizers = postProductDto.PostProductLocalizers
                .Select(p =>
                {
                    var r = _mapper.Map<ProductLocalizer>(p);
                    r.ProductId = product.Id;
                    return r;
                })
                .ToList();
            }                 
            _dataContext.Products.Add(product);
            await _dataContext.SaveChangesAsync();
            GetProductDto getProductDto = _mapper.Map<GetProductDto>(product);
            if(product.Localizers != null) 
                getProductDto.GetProductLocalizers = product.Localizers
                    .Select(l=>_mapper.Map<GetProductLocalizerDto>(l))
                    .ToList();
            if(product.Images != null)
                getProductDto.GetProductImageDtos = product.Images
                    .Select(i=>_mapper.Map<GetProductImageDto>(i))
                    .ToList();
            return getProductDto;
        }
        public GetProductDto? GetProductDtoById(Guid id)
        {
            Product? product = _dataContext.Products.FirstOrDefault(p => p.Id == id);
            if (product == null)
                return null;
            GetProductDto getProductDto = _mapper.Map<GetProductDto>(product);
            
            getProductDto.GetProductLocalizers = product.Localizers?.Select(l=>_mapper.Map<GetProductLocalizerDto>(l)).ToList();
            getProductDto.GetProductImageDtos = product.Images?.Select(i=>_mapper.Map<GetProductImageDto>(i)).ToList();
            getProductDto.GetVariantDtos = product.Variants?.Select(p=>_mapper.Map<GetVariantDto>(p)).ToList();
            return getProductDto;
        }
        public Product? GetProductById(Guid id)
        {
            return _dataContext.Products.FirstOrDefault(i => i.Id == id);
        }
        public GetProductDto UpdateProductInformation(Product product, PutProductDto putProductDto)
        {
            _mapper.Map(putProductDto, product);
            _dataContext.Products.Update(product);
            _dataContext.SaveChanges();
            return _mapper.Map<GetProductDto>(product);
        }
        public void DeleteProduct(Product product)
        {
            List<ProductImage> productImages = _dataContext.ProductImages.Where(p => p.ProductId == product.Id).ToList();
            _dataContext.ProductImages.RemoveRange(productImages);
            List<ProductLocalizer> productLocalizers = _dataContext.ProductLocalizers.Where(p=>p.ProductId == product.Id).ToList();
            _dataContext.ProductLocalizers.RemoveRange(productLocalizers);
            _dataContext.Products.Remove(product);
            _dataContext.SaveChanges();
        }
        public async Task<GetProductImageDto?> AddProductImage(Guid ProductId,PostProductImageDto postProductImageDto)
        {
            if (!IsImageValid(postProductImageDto.Image))
                return null;
            string imageUrl = await SaveImage(postProductImageDto.Image);
            ProductImage productImage = _mapper.Map<ProductImage>(postProductImageDto);
            productImage.Url = imageUrl;
            productImage.ProductId = ProductId;
            _dataContext.ProductImages.Add(productImage);
            await _dataContext.SaveChangesAsync();
            return _mapper.Map<GetProductImageDto>(productImage);
        }
        public GetProductLocalizerDto AddProductLocalizer(Guid ProductId,PostProductLocalizerDto postProductLocalizerDto)
        {
            ProductLocalizer productLocalizer = _mapper.Map<ProductLocalizer>(postProductLocalizerDto);
            productLocalizer.ProductId = ProductId;
            _dataContext.ProductLocalizers.Add(productLocalizer);
            _dataContext.SaveChanges();
            return _mapper.Map<GetProductLocalizerDto>(productLocalizer);
        }
        public async Task<List<GetProductImageDto>> GetProductImagesForProduct(Guid productId)
        {
            return await _dataContext.ProductImages
                .Where(p=>p.ProductId == productId)
                .Select(p=>_mapper.Map<GetProductImageDto>(p))
                .ToListAsync();
        }
        public GetProductImageDto? GetProductImageDtoById(Guid id)
        {
            return _dataContext.ProductImages
                .Where(p => p.Id == id)
                .Select(p => _mapper.Map<GetProductImageDto>(p))
                .FirstOrDefault();
        }
        public ProductImage? GetProductImageById(Guid id)
        {
            return _dataContext.ProductImages.FirstOrDefault(p => p.Id == id);
        }
        public async Task<List<GetProductLocalizerDto>> GetProductLocalizersForProduct(Guid productId)
        {
            return await _dataContext.ProductLocalizers
                .Where(p=>p.ProductId == productId)
                .Select(p=>_mapper.Map<GetProductLocalizerDto>(p))
                .ToListAsync();
        }
        public GetProductLocalizerDto? GetProductLocalizerDtoById(Guid id)
        {
            return _dataContext.ProductLocalizers
                .Where(p=>p.Id == id)
                .Select(p => _mapper.Map<GetProductLocalizerDto>(p))
                .FirstOrDefault();
        }
        public ProductLocalizer? GetProductLocalizerById(Guid id)
        {
            return  _dataContext.ProductLocalizers.FirstOrDefault(p => p.Id == id);
        }
        public void DeleteProductImage(ProductImage productImage)
        {
            _dataContext.ProductImages.Remove(productImage);
            _dataContext.SaveChanges();
        }
        public void DeleteProductLolizer(ProductLocalizer productLocalizer)
        {
            _dataContext.ProductLocalizers.Remove(productLocalizer);
            _dataContext.SaveChanges();
        }
        public GetProductLocalizerDto UpdateProductLocalizerInformation(ProductLocalizer productLocalizer,PutProductLocalizerDto putProductLocalizerDto)
        {
            _mapper.Map(putProductLocalizerDto, productLocalizer);
            _dataContext.ProductLocalizers.Update(productLocalizer);
            _dataContext.SaveChanges();
            return _mapper.Map<GetProductLocalizerDto>(productLocalizer);
        }
        public List<GetProductDto> GetProducts(FilterModel filterModel)
        {
            List<Product> products = _dataContext.Products
                .Include(P=>P.Images)
                .Include(p=>p.Variants)
                .ThenInclude(v=>v.Properties)
                .ToList();
            if(filterModel.HasImage)
                products = products.Where(p=>p.Images.Any()).ToList();
            else
                products = products.Where(p => p.Images.Any()).ToList();
            if(!string.IsNullOrEmpty(filterModel.SearchQuery)) 
                products = products.Where(p=>p.Name.ToLower().Contains(filterModel.SearchQuery.ToLower()) ||
                p.Code.ToLower().Contains(filterModel.SearchQuery.ToLower())||
                p.Description.ToLower().Contains(filterModel.SearchQuery.ToLower()))
                    .ToList();
            if(filterModel.Properties !=null && filterModel.Properties.Any())          
                products = products.
                    Where(p => p.Variants.
                    Any(v => v.Properties.
                    Any(pr => filterModel.Properties.Contains(pr.PropertyItemId)))).ToList();
            
            if(filterModel.Sort != null && filterModel.Sort == "desc") 
                products = products.OrderByDescending(p=>p.Code).ThenByDescending(p=>p.Name).ToList();
            else
                products = products.OrderBy(p => p.Code).ThenBy(p => p.Name).ToList();
            products = products.Skip(filterModel.PageSize * filterModel.PageIndex).Take(filterModel.PageSize).ToList();
            List<GetProductDto> result = products.Select(product =>
            {
                GetProductDto getProductDto = _mapper.Map<GetProductDto>(product);
                getProductDto.GetProductLocalizers = product.Localizers.Select(l => _mapper.Map<GetProductLocalizerDto>(l)).ToList();
                getProductDto.GetProductImageDtos = product.Images.Select(i => _mapper.Map<GetProductImageDto>(i)).ToList();
                getProductDto.GetVariantDtos = product.Variants.Select(p => _mapper.Map<GetVariantDto>(p)).ToList();
                return getProductDto;
            }).ToList();
            return result; 
        }
        public void MakeImagesNotMainForProductExceptImage(Guid productId,Guid imageId)
        {
            _dataContext.ProductImages
                .Where(p => p.ProductId == productId && p.Id != imageId)
                .ToList()
                .ForEach(p => p.IsMainImage = false);
            _dataContext.SaveChanges();
        }
    }
}
