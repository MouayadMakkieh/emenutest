﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MouayadEmenu.Data;
using MouayadEmenu.Models;

namespace MouayadEmenu.repositories
{
    public class VariantRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        public VariantRepository(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        public GetVariantDto AddVariant(PostVariantDto postVariantDto)
        {
            Variant variant = _mapper.Map<Variant>(postVariantDto);
            variant.Id = Guid.NewGuid();
            List<ProductImage> productImages = _dataContext.ProductImages
                .Where(p => postVariantDto.ImagesId.Contains(p.Id))
                .ToList();
            productImages.ForEach(p => p.VariantId = variant.Id);

            List<VariantProperty> variantProperties = postVariantDto.PropertyItemsId.Select(p => new VariantProperty
            {
                PropertyItemId = p,
                VariantId = variant.Id
            }).ToList();
            variant.Properties.AddRange(variantProperties);
            _dataContext.Variants.Add(variant);
            _dataContext.SaveChanges();
            GetVariantDto getVariantDto = GetVariantDtoById(variant.Id);
            return getVariantDto;
        }
        public GetVariantDto? GetVariantDtoById(Guid variantId)
        {
            Variant? variant = _dataContext.Variants.FirstOrDefault(p => p.Id == variantId);
            if (variant == null)
                return null;
            GetVariantDto getVariantDto = _mapper.Map<GetVariantDto>(variant);
            getVariantDto.ProductImageDtos = _dataContext.ProductImages.Select(p => _mapper.Map<GetProductImageDto>(p)).ToList();
            getVariantDto.VariantPropertyDtos = _dataContext.VariantProperties.Select(v => _mapper.Map<GetVariantPropertyDto>(v)).ToList();
            return getVariantDto;
        }
        public Variant? GetVariantById(Guid variantId)
        {
            return _dataContext.Variants.FirstOrDefault(v => v.Id == variantId);
        }
        public GetVariantDto UpdateVariantInformation(Variant variant, PostVariantDto postvariantDto)
        {
            _mapper.Map(postvariantDto, variant);
            _dataContext.Update(variant);
            _dataContext.SaveChanges();
            return _mapper.Map<GetVariantDto>(variant);
        }
        public void DeleteVariant(Variant variant)
        {
            _dataContext.Variants.Remove(variant);
            _dataContext.SaveChanges();
        }
        public GetProductImageDto AddImageForVariant(Guid id,ProductImage image)
        {
            image.VariantId = id;
            _dataContext.Update(image);
            _dataContext.SaveChanges();
            return _mapper.Map<GetProductImageDto>(image);
        }
        public GetVariantPropertyDto AddPropertyItemForVariant(PostVariantPropertyDto postvariantPropertyDto)
        {
            VariantProperty variantProperty = _mapper.Map<VariantProperty>(postvariantPropertyDto);
            _dataContext.VariantProperties.Add(variantProperty);
            _dataContext.SaveChanges();
            return _mapper.Map<GetVariantPropertyDto>(variantProperty);
        }
        public async Task<List<GetProductImageDto>> GetProductImagesForVariant(Guid VariantId)
        {
            return await _dataContext.ProductImages
                .Where(p=>p.VariantId == VariantId)
                .Select(p=>_mapper.Map<GetProductImageDto>(p))
                .ToListAsync();
        }
        public async Task<List<GetPropertyItemDto>> GetPropertyItemsForVariant(Guid VariantId)
        {
            return await _dataContext.PropertyItems
                .Include(p=>p.VariantProperties)
                .Where(p=>p.VariantProperties.Any(v=>v.VariantId == VariantId))
                .Select(p=>_mapper.Map<GetPropertyItemDto>(p))
                .ToListAsync();
        }
        public VariantProperty? GetVariantProperty(Guid variantId,Guid propertyItemId)
        {
            return _dataContext.VariantProperties
                .FirstOrDefault(v=>v.VariantId == variantId && v.PropertyItemId == propertyItemId);
        }
        public void DeletePropertyItemForVariant(VariantProperty variantProperty)
        {
            _dataContext.VariantProperties.Remove(variantProperty);
            _dataContext.SaveChanges();
        }
        public GetProductImageDto DeleteImageForVariant(ProductImage image)
        {
            image.VariantId = null;
            _dataContext.Update(image);
            _dataContext.SaveChanges();
            return _mapper.Map<GetProductImageDto>(image);  
        }
        public async Task<List<GetVariantDto>> GetVariants(FilterModel filterModel)
        {
            List<Variant> variants = await _dataContext.Variants
                .Include(p=>p.Properties)
                .Include(p=>p.Images)
                .ToListAsync();
            if(filterModel.HasImage)
                variants = variants.Where(v=>v.Images.Any()).ToList();
            else
                variants = variants.Where(v => !v.Images.Any()).ToList();
            if(!string.IsNullOrEmpty(filterModel.SearchQuery))
                variants = variants
                .Where(v => v.Name.ToLower().Contains(filterModel.SearchQuery.ToLower())||
                v.Description.ToLower().Contains(filterModel.SearchQuery.ToLower())).ToList();

            if(filterModel.Properties !=null && filterModel.Properties.Any())
                variants = variants.Where(v=>v.Properties
                .Any(pr => filterModel.Properties.Contains(pr.PropertyItemId)))
                    .ToList();
            if (filterModel.Sort != null && filterModel.Sort == "desc")
                variants = variants.OrderByDescending(p => p.Name).ToList();
            else
                variants = variants.OrderBy(p => p.Name).ToList();
            variants = variants = variants.Skip(filterModel.PageSize * filterModel.PageIndex).Take(filterModel.PageSize).ToList();
            List<GetVariantDto> result = variants.Select(v =>
            {
                GetVariantDto getVariantDto = _mapper.Map<GetVariantDto>(v);
                getVariantDto.VariantPropertyDtos = v.Properties.Select(p => _mapper.Map<GetVariantPropertyDto>(p)).ToList();
                getVariantDto.ProductImageDtos = v.Images.Select(i => _mapper.Map<GetProductImageDto>(i)).ToList();
                return getVariantDto;
            }).ToList();
            return result;
        }

    }
}
