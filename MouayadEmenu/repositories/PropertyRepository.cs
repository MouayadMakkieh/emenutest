﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MouayadEmenu.Data;
using MouayadEmenu.Models;
namespace MouayadEmenu.repositories
{
    public class PropertyRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public PropertyRepository(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        public async Task<List<GetPropertyDto>> GetAllProperty()
        {
            return await _dataContext.Properties
                .Select(p => _mapper.Map<GetPropertyDto>(p))
                .ToListAsync();
        }
        public Property? GetPropertyById(Guid id)
        {
            return _dataContext.Properties.FirstOrDefault(p=>p.Id == id);
        }
        public GetPropertyDto? GetPropertyDtoById(Guid id)
        {
            return _dataContext.Properties
                .Where(p => p.Id == id)
                .Select(p => _mapper.Map<GetPropertyDto>(p))
                .FirstOrDefault();
        }
        public GetPropertyDto AddNewProperty(string PropertyName)
        {
            Property property = new Property { Name = PropertyName };
            _dataContext.Properties.Add(property);
            _dataContext.SaveChanges();
            return _mapper.Map<GetPropertyDto>(property);
        }
        public GetPropertyDto UpdatePropertyName(Property property, string PropertyName)
        {
            property.Name = PropertyName;
            _dataContext.Properties.Update(property);
            _dataContext.SaveChanges();
            return _mapper.Map<GetPropertyDto>(property);
        }
        public void DeleteProperty(Property property)
        {
            _dataContext.Properties.Remove(property);
            _dataContext.SaveChanges();
        }
        public async Task<List<GetPropertyItemDto>> GetPropertyItemsByPropertyId(Guid id)
        {
            return await _dataContext.PropertyItems.Where(p => p.Propertyid == id)
                .Select(p => _mapper.Map<GetPropertyItemDto>(p))
                .ToListAsync();
        }
        public PropertyItem? GetPropertyItemById(Guid id)
        {
            return _dataContext.PropertyItems.FirstOrDefault(p => p.Id == id);
        }
        public GetPropertyItemDto? GetPropertyItemDtoById(Guid id)
        {
            return _dataContext.PropertyItems
                .Where(p => p.Id == id)
                .Select(p => _mapper.Map<GetPropertyItemDto>(p))
                .FirstOrDefault();
        }
        public GetPropertyItemDto AddNewPropertyItem(PostPropertyItemDto postPropertyItemDto)
        {
            PropertyItem propertyItem = _mapper.Map<PropertyItem>(postPropertyItemDto);
            _dataContext.PropertyItems.Add(propertyItem);
            _dataContext.SaveChanges();
            return _mapper.Map<GetPropertyItemDto>(propertyItem);
        }
        public GetPropertyItemDto UpdatePropertyItemInformation(PropertyItem propertyItem, PostPropertyItemDto postPropertyItemDto)
        {
            _mapper.Map(postPropertyItemDto, propertyItem);
            _dataContext.PropertyItems.Update(propertyItem);
            _dataContext.SaveChanges();
            return _mapper.Map<GetPropertyItemDto>(propertyItem);
        }
        public void DeletePropertyItem(PropertyItem propertyItem) 
        {
            _dataContext.PropertyItems.Remove(propertyItem);
            _dataContext.SaveChanges();
        }
    }

}
