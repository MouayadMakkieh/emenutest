﻿using Microsoft.AspNetCore.Mvc;
using MouayadEmenu.Models;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private readonly IlanguageService _languageService;
        public LanguageController(IlanguageService languageService)
        {
           _languageService = languageService;
        }
        [HttpGet("GetLanguageById/{id}")]
        public ActionResult GetLanguageById(Guid id)
        {
            var result =  _languageService.GetLanguageById(id);
            if (!result.success)
                return NotFound(result);
            return Ok(result);
        }
        [HttpGet("GetAllLanguages")]
        public async Task<ActionResult> GetAllLanguages()
        {
            var result = await _languageService.GetAllLanguages();
            if(!result.success)
                return NoContent();
            return Ok(result);
        }
        [HttpPost("PostLanguage")]
        public ActionResult PostLanguage([FromBody]PostLanguageDto postLanguage)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _languageService.PostLanguage(postLanguage);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPut("PutLanguage/{id}")]
        public ActionResult PutLanguage(Guid id,[FromBody] PostLanguageDto postLanguage)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _languageService.PutLanguage(id, postLanguage);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteLanguage/{id}")]
        public ActionResult DeleteLanguage(Guid id)
        {
            var result = _languageService.DeleteLanguage(id);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
    }
}
