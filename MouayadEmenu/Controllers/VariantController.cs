﻿using Microsoft.AspNetCore.Mvc;
using MouayadEmenu.Models;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VariantController : ControllerBase
    {
        private readonly IVariantService _variantService;

        public VariantController(IVariantService variantService)
        {
            _variantService = variantService;
        }
        [HttpPost("PostVariant")]
        public ActionResult PostVariant(PostVariantDto postVariantDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _variantService.PostVariant(postVariantDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetVariantById/{id}")]
        public ActionResult GetVariantById(Guid id)
        {
            var result = _variantService.GetVariantById(id);
            if (!result.success)
                return NotFound(result);
            return Ok(result);
        }
        [HttpGet("GetVariants")]
        public async Task<ActionResult> GetVariants([FromQuery]FilterModel filterModel)
        {
            var result = await _variantService.GetVariants(filterModel);
            if (!result.Success)
                return NoContent();
            return Ok(result);
        }
        [HttpPut("PutVariant/{id}")]
        public ActionResult PutVariant(Guid id, [FromBody] PostVariantDto postVariantDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _variantService.PutVariant(id, postVariantDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteVariant/{id}")]
        public ActionResult DeleteVariant(Guid id)
        {
            var result = _variantService.DeleteVariant(id);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPost("AddImageForVariant")]
        public ActionResult AddImageForVariant(Guid VariantId, Guid imageId)
        {
            var result = _variantService.AddImageForVariant(VariantId, imageId);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPost("AddPropertyItemForVariant")]
        public ActionResult AddPropertyItemForVariant(PostVariantPropertyDto postVariantPropertyDto)
        {
            var result = _variantService.AddPropertyItemForVariant(postVariantPropertyDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeletePropertyItemForVariant/{variantId}")]
        public ActionResult DeletePropertyItemForVariant(Guid variantId, Guid propertyItemId)
        {
            var result = _variantService.DeleteVariantProperty(variantId, propertyItemId);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetImagesByVariantId/{variantId}")]
        public async  Task<ActionResult> GetImagesByVariantId(Guid variantId)
        {
            var result = await _variantService.GetImagesByVariantId(variantId);
            if (!result.success && string.IsNullOrEmpty(result.message))
                return NoContent();
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("RemoveImageFromVariant/{imageId}")]
        public ActionResult RemoveImageFromVariant(Guid imageId)
        {
            var result = _variantService.RemoveImageFromVariant(imageId);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetPropertyItemsByVariantId/{variantId}")]
        public async Task<ActionResult> GetPropertyItemsByVariantId(Guid variantId)
        {
            var result = await _variantService.GetPropertyItemsByVariantId(variantId);
            if (!result.success && string.IsNullOrEmpty(result.message))
                return NoContent();
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
    }
}
