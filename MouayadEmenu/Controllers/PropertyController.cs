﻿using Microsoft.AspNetCore.Mvc;
using MouayadEmenu.Models;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertyController : ControllerBase
    {
        private readonly IPropertyService _propertyService;

        public PropertyController(IPropertyService propertyService)
        {
            _propertyService = propertyService;
        }

        [HttpGet("GetAllProperties")]
        public async Task<ActionResult> GetAllProperties()
        {
            var result = await _propertyService.GetAllProperties();
            if (!result.success)
                return NoContent();
            return Ok(result);
        }
        [HttpGet("GetPropertyById/{id}")]
        public ActionResult GetPropertyById(Guid id)
        {
            var result = _propertyService.GetPropertyById(id);
            if (!result.success)
                return NotFound(result);
            return Ok(result);
        }
        [HttpPost("PostProperty")]
        public ActionResult PostProperty([FromBody]string propertyName)
        {
            var result = _propertyService.PostProperty(propertyName);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPut("PutProperty/{id}")]
        public ActionResult PutProperty(Guid id, [FromBody]string propertyName)
        {
            var result = _propertyService.PutProperty(id, propertyName);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteProperty/{id}")]
        public ActionResult DeleteProperty(Guid id)
        {
            var result = _propertyService.DeleteProperty(id);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetPropertyItemsByPropertyId/{id}")]
        public async Task<ActionResult> GetPropertyItemsByPropertyId(Guid id)
        {
            var result = await _propertyService.GetPropertyItemsByPropertyId(id);
            if (!result.success && string.IsNullOrEmpty(result.message))
                return NoContent();
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetPropertyItemById/{id}")]
        public ActionResult GetPropertyItemById(Guid id)
        {
            var result = _propertyService.GetPropertyItemById(id);
            if (!result.success)
                return NotFound(result);
            return Ok(result);
        }
        [HttpPost("PostPropertyItem")]
        public ActionResult PostPropertyItem(PostPropertyItemDto postPropertyItemDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _propertyService.PostPropertyItem(postPropertyItemDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPut("PutPropertyItem/{id}")]
        public ActionResult PutPropertyItem(Guid id, PostPropertyItemDto postPropertyItemDto)
        {
            var result = _propertyService.PutPropertyItem(id, postPropertyItemDto);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeletePropertyItem/{id}")]
        public ActionResult DeletePropertyItem(Guid id)
        {
            var result = _propertyService.DeletePropertyItem(id);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
    }
}
