﻿using Microsoft.AspNetCore.Mvc;
using MouayadEmenu.Models;
using MouayadEmenu.Services.Interfaces;

namespace MouayadEmenu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpPost("PostProduct")]
        public async Task<ActionResult> PostProduct([FromForm]PostProductDto postProductDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = await _productService.PostProduct(postProductDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetProductById/{id}")]
        public ActionResult GetProductById(Guid id) 
        {
            var result = _productService.GetProductById(id);
            if (!result.success)
                return NotFound(result);
            return Ok(result);
        }
        [HttpGet("GetProducts")]
        public ActionResult GetProducts([FromQuery]FilterModel filterModel) 
        {
            var result = _productService.GetProducts(filterModel);
            if(!result.Success)
                return NoContent();
            return Ok(result);
        }
        [HttpPut("PutProduct/{id}")]
        public ActionResult PutProduct(Guid id, [FromBody] PutProductDto putProductDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _productService.PutProduct(id, putProductDto);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteProduct/{id}")]
        public ActionResult DeleteProduct(Guid id)
        {
            var result = _productService.DeleteProduct(id);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPost("PostProductImage")]
        public async Task<ActionResult> PostProductImage(Guid ProductId,[FromForm]PostProductImageDto postProductImageDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = await _productService.PostProductImage(ProductId,postProductImageDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetPoductImageById/{id}")]
        public ActionResult GetPoductImageById(Guid id)
        {
            var result = _productService.GetPoductImageById(id);
            if(!result.success)
                return NotFound(result);
            return Ok(result);
        }
        [HttpGet("GetProductImagesByProductId/{id}")]
        public async Task<ActionResult> GetProductImagesByProductId(Guid id)
        {
            var result = await _productService.GetProductImagesByProductId(id);
            if (!result.success && string.IsNullOrEmpty(result.message))
                return NoContent();
            if(!result.success) 
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteProductImage/{id}")]
        public ActionResult DeleteProductImage(Guid id)
        {
            var result = _productService.DeleteProductImage(id);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPost("PostProductLocalizer")]
        public ActionResult PostProductLocalizer(Guid ProductId,PostProductLocalizerDto postProductLocalizerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _productService.PostProductLocalizer(ProductId,postProductLocalizerDto);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetProductLocalizerById/{id}")]
        public ActionResult GetProductLocalizerById(Guid id)
        {
            var result = _productService.GetProductLocalizerById(id);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpGet("GetProductLocalizersByProductId/{id}")]
        public async  Task<ActionResult> GetProductLocalizersByProductId(Guid id)
        {
            var result = await _productService.GetProductLocalizersByProductId(id);
            if (!result.success && string.IsNullOrEmpty(result.message))
                return NoContent();
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpPut("PutProductLocalizer")]
        public ActionResult PutProductLocalizer(Guid id, PutProductLocalizerDto putProductLocalizerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList());
            }
            var result = _productService.PutProductLocalizer(id, putProductLocalizerDto);
            if(!result.success)
                return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteProductLocalizer/{id}")]
        public ActionResult DeleteProductLocalizer(Guid id)
        {
            var result = _productService.DeleteProductLocalizer(id);
            if (!result.success)
                return BadRequest(result);
            return Ok(result);
        }

    }
}
