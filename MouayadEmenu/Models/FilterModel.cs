﻿namespace MouayadEmenu.Models
{
    public class FilterModel
    {
        public string? SearchQuery { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string? Sort { get; set; }
        public List<Guid>? Properties { get; set; }
        public bool HasImage { get; set; } = true;
    }
}
