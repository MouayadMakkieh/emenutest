﻿namespace MouayadEmenu.Models
{
    public class PaginationServiceResponse
    {
        public bool Success { get; set; }
        public string? Message { get; set; }
        public object? Data { get; set; }
        public int TotalPages { get; set; }
        public int DataSize { get; set; }
    }
}
