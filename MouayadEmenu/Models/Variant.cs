﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MouayadEmenu.Models
{
    public class Variant
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [Required]
        [ForeignKey(nameof(Product))]
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public List<ProductImage> Images { get; set; } = new List<ProductImage>();
        public List<VariantProperty> Properties { get; set; } = new List<VariantProperty>();
    }
    public class GetVariantDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ProductId { get; set; }
        public List<GetProductImageDto> ProductImageDtos { get; set; } = new List<GetProductImageDto>();
        public List<GetVariantPropertyDto> VariantPropertyDtos { get; set; } = new List<GetVariantPropertyDto>();

    }
    public class PostVariantDto
    {
        public string Name { get; set; }
        public string? Description { get; set; }
        public Guid ProductId { get; set; }
        public List<Guid> ImagesId { get; set; } = new List<Guid>();
        public List<Guid> PropertyItemsId { get; set; } = new List<Guid>();
    }
}
