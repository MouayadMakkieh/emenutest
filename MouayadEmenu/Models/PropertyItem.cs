﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MouayadEmenu.Models
{
    public class PropertyItem
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [ForeignKey(nameof(Property))]
        public Guid Propertyid { get; set; }
        public Property Property { get; set; }
        public List<VariantProperty> VariantProperties { get; set;}
    }
    public class GetPropertyItemDto
    {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Guid Propertyid { get; set; }
    }
    public class PostPropertyItemDto
    {
        public string Name { get; set; }
        public Guid Propertyid { get; set; }
    }
}
