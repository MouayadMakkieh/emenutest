﻿using System.ComponentModel.DataAnnotations;

namespace MouayadEmenu.Models
{
    public class Language
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(50)]
        public string NativeName { get; set; }
        [Required]
        [StringLength(50)]
        public string EnglishName { get; set; }
        [Required]
        [StringLength(50)]
        public string LanguageCode { get; set; }
        public List<ProductLocalizer> Localizers { get; set; }
    }
    public class GetLanguageDto
    {
        public Guid Id { get; set; }
        public string NativeName { get; set; }
        public string EnglishName { get; set; }
        public string LanguageCode { get; set; }
    }
    public class PostLanguageDto
    {
        public string NativeName { get; set; }
        public string EnglishName { get; set; }
        public string LanguageCode { get; set; }
    }
}
