﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MouayadEmenu.Models
{
    public class ProductLocalizer
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey(nameof(Product))]
        public Guid ProductId { get; set; }
        [Required]
        [ForeignKey(nameof(Language))]
        public Guid LanguageId { get; set; }
        [Required]
        [StringLength(100)]
        public string LoacalizeName { get; set; }
        [Required]
        [StringLength(200)]
        public string LoacalizeDescription { get; set; }
        public Language Language { get; set; }
        public Product Product { get; set; }
    }
    public class GetProductLocalizerDto
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid LanguageId { get; set; }
        public string LoacalizeName { get; set; }
        public string LoacalizeDescription { get; set; }
    }
    public class PostProductLocalizerDto
    {      
        public Guid LanguageId { get; set; }
        public string LoacalizeName { get; set; }
        public string LoacalizeDescription { get; set; }
    }
    public class PutProductLocalizerDto
    {
        public Guid ProductId { get; set; }
        public Guid LanguageId { get; set; }
        public string LoacalizeName { get; set; }
        public string LoacalizeDescription { get; set; }
    } 
}
