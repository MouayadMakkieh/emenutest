﻿using System.ComponentModel.DataAnnotations;

namespace MouayadEmenu.Models
{
    public class Product
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        public string Code { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public List<Variant> Variants { get; set; } = new List<Variant>();
        public List<ProductImage> Images { get; set; } = new List<ProductImage>();
        public List<ProductLocalizer> Localizers { get; set; } = new List<ProductLocalizer>();

    }
    public class GetProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public List<GetVariantDto> GetVariantDtos { get; set; } = new List<GetVariantDto>();
        public List<GetProductImageDto> GetProductImageDtos { get; set; } = new List<GetProductImageDto>();
        public List<GetProductLocalizerDto> GetProductLocalizers { get; set; } = new List<GetProductLocalizerDto>();

    }
    public class PostProductDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public List<PostProductLocalizerDto> PostProductLocalizers { get; set; } = new List<PostProductLocalizerDto>();
        public PostProductImageDto? PostProductImageDto { get; set; } = new PostProductImageDto();
    }
    public class PutProductDto
    {
        public string Name { get; set; }
        public string? Description { get; set; }
        public string Code { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
