﻿using System.ComponentModel.DataAnnotations;

namespace MouayadEmenu.Models
{
    public class Property
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public List<PropertyItem> Items { get; set; }
    }
    public class GetPropertyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
