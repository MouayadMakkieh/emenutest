﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MouayadEmenu.Models
{
    public class ProductImage
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Url { get; set; }
        public string Title { get; set; } =  string.Empty;
        public string Description { get; set; } = string.Empty;
        public bool IsMainImage { get; set; } = false;
        [ForeignKey(nameof(Product))]
        public Guid ProductId { get; set; }
        [ForeignKey(nameof(Variant))]
        public Guid? VariantId { get; set; }
        public Product Product { get; set; }
        public Variant Variant { get; set; }
    }
    public class GetProductImageDto
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public bool IsMainImage { get; set; } = false;
        public Guid ProductId { get; set; }
        public Guid? VariantId { get; set; }
    }
    public class PostProductImageDto
    {
        public IFormFile Image { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public bool IsMainImage { get; set; } = false;
        public Guid? VariantId { get; set; }
    }
}
