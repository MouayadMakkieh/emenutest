﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MouayadEmenu.Models
{
    public class VariantProperty
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey(nameof(Variant))]
        public Guid VariantId { get; set; }
        [Required]
        [ForeignKey(nameof(PropertyItem))]
        public Guid PropertyItemId { get; set; }
        public Variant Variant { get; set; }
        public PropertyItem PropertyItem { get; set; }

    }
    public class GetVariantPropertyDto
    {
        public Guid Id { get; set; }
        public Guid VariantId { get; set; }
        public Guid PropertyItemId { get; set; }
    }
    public class PostVariantPropertyDto
    {
        public Guid VariantId { get; set; }
        public Guid PropertyItemId { get; set; }
    }
}
