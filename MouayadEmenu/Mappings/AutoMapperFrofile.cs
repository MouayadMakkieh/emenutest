﻿using AutoMapper;
using MouayadEmenu.Models;

namespace MouayadEmenu.Mappings
{
    public class AutoMapperFrofile : Profile
    {
        public AutoMapperFrofile()
        {
            CreateMap<PostLanguageDto, Language>();
            CreateMap<Language, GetLanguageDto>();
            CreateMap<PostProductDto, Product>();
            CreateMap<Product, GetProductDto>();
            CreateMap<PostVariantDto, Variant>();
            CreateMap<Variant, GetVariantDto>();
            CreateMap<Property,GetPropertyDto>();
            CreateMap<PostPropertyItemDto, PropertyItem>();
            CreateMap<PropertyItem,GetPropertyItemDto>();
            CreateMap<PostProductImageDto, ProductImage>();
            CreateMap<ProductImage, GetProductImageDto>();
            CreateMap<PostProductLocalizerDto, ProductLocalizer>();
            CreateMap<PutProductLocalizerDto,ProductLocalizer>();
            CreateMap<ProductLocalizer, GetProductLocalizerDto>();
            CreateMap<PostVariantPropertyDto, VariantProperty>();
            CreateMap<VariantProperty, GetVariantPropertyDto>();
            CreateMap<PutProductDto, Product>();
        }
    }
}
